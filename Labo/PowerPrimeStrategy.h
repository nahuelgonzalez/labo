#pragma once
#include "AlgebraicGroupPrimeStrategy.h"

class PowerPrimeStrategy :
	public AlgebraicGroupPrimeStrategy
{
public:
	virtual long Next(long* e);
	virtual long BeginSecondStage();
	virtual long GetLastPrime();

	PowerPrimeStrategy(long interval = 1000, double ratio = 3);
	~PowerPrimeStrategy(void);

	virtual char* ReportProgress(char* tmp);

private:
	long	interval;
	double	ratio;
	long	max_prime;
	int		max_e;
	bool	second_stage;
};

