#pragma once

#include "NTL/ZZ.h"
#include "NTL/ZZ_p.h"

using namespace NTL;


#include "FactorizationAlgorithm.h"
#include "AlgebraicGroupFactorizationAlgorithm.h"
#include "AlgebraicGroupPrimeStrategy.h"


class PollardPMinusOne : public AlgebraicGroupFactorizationAlgorithm
{
public:
	PollardPMinusOne(ZZ& N, AlgebraicGroupPrimeStrategy& s, ZZ& residue = to_ZZ(2));
	~PollardPMinusOne(void);

	virtual ZZ FirstStage  ( long limit = 0 );
	virtual ZZ SecondStage ( long limit = 0 );
	ZZ ImprovedSecondStage ( long w, long limit = 0 );

private:
	ZZ_p residue;
};

