#include "AlgebraicGroupFactorizationAlgorithm.h"
#include "Log.h"
#include "exception"
#include "conio.h"

using namespace std;


AlgebraicGroupFactorizationAlgorithm::AlgebraicGroupFactorizationAlgorithm(ZZ& N, AlgebraicGroupPrimeStrategy& s, int gcd_skip)
	: FactorizationAlgorithm(N,10000), s(s), gcd_skip(gcd_skip)
{
	report_count = gcd_count = 0;
	base_report_skip = report_skip = 10000;

	last_ticks = MSTIMER();
}


AlgebraicGroupFactorizationAlgorithm::~AlgebraicGroupFactorizationAlgorithm(void)
{
}


bool AlgebraicGroupFactorizationAlgorithm::CheckGCD(ZZ& retval, ZZ_p& acc)
{
	GCD(retval, N, rep(acc));
	if ( !IsOne(retval) )
	{
		if ( retval != N )
			return true;
		else
		{
			// TODO: ACA HABRIA QUE VOLVER ATRAS
			int k = 9;
			throw new exception("ACA HABRIA QUE VOLVER ATRAS");
		}
	}

	return false;
}


#include "iostream"
using namespace std;


bool AlgebraicGroupFactorizationAlgorithm::AccumulateGCD(ZZ& retval, ZZ_p& residue)
{
	bool stop = false;
	gcd_count++;
	if ( gcd_count > gcd_skip )
	{
		gcd_count = 0;

		if ( IsOne(residue) || IsZero(residue) )
		{
			int k = 0;
			LOG("ERROR!!! ERROR!!! AccumulateGCD");
			getch();
		} 

		stop = CheckGCD(retval, residue);
	}

	return stop;
}


void AlgebraicGroupFactorizationAlgorithm::ReportProgress()
{
	report_count++;
	if ( report_count > report_skip )
	{
		report_count = 0;

		TICKS ticks = MSTIMER();
		unsigned long ops = 1000 * s.GetOpCount() / (ticks - last_ticks);
		last_ticks = ticks;

		char tmp[100], tmp2[100];
		LOG("    " << s.ReportProgress(tmp2) << " - " << human_readable(tmp,ops) << "ops/sec");
	}
}


void AlgebraicGroupFactorizationAlgorithm::SetProgressSkip(double factor)
{
	report_skip = factor * base_report_skip;
}
