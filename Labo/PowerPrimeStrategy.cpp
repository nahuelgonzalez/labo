#include "PowerPrimeStrategy.h"


PowerPrimeStrategy::PowerPrimeStrategy(long interval, double ratio)
	: interval(interval), ratio(ratio), max_prime(0)
{
	max_e = 1;
	second_stage = false;
}


PowerPrimeStrategy::~PowerPrimeStrategy(void)
{
}


#include "math.h"

long PowerPrimeStrategy::Next(long* e)
{
	op_count++;

	*e = 1;
	last_prime = seq.next();
	if ( last_prime > max_prime )
	{
		max_prime = last_prime;
		if ( last_prime > interval )
		{
			//interval = (long) pow((double) interval, ratio);
			interval = (long) (interval * ratio);
			seq.reset(0);
			max_e++;
		}
	}

	return last_prime;
}


long PowerPrimeStrategy::BeginSecondStage()
{
	op_count = 0;
	second_stage = true;
	last_prime = max_prime;
	seq.reset(last_prime+1);
	return last_prime;
}


long PowerPrimeStrategy::GetLastPrime()
{
	if ( max_prime > last_prime )
		return max_prime;
	else
		return last_prime;
}


char* PowerPrimeStrategy::ReportProgress(char* tmp)
{
	if ( !second_stage )
		sprintf ( tmp , "p = %ld/%d (%ld)" , max_prime , max_e , last_prime );
	else
		sprintf ( tmp , "p = %ld" , last_prime );

	return tmp;
}
