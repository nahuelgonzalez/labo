#pragma once

#include "NTL/ZZ.h"
#include "NTL/ZZ_p.h"

using namespace NTL;


#include "FactorizationAlgorithm.h"
#include "AlgebraicGroupPrimeStrategy.h"

#include "util.h"


class AlgebraicGroupFactorizationAlgorithm : public FactorizationAlgorithm
{
public:
	AlgebraicGroupFactorizationAlgorithm(ZZ& N, AlgebraicGroupPrimeStrategy& s, int gcd_skip = 1000);
	~AlgebraicGroupFactorizationAlgorithm(void);

	virtual ZZ FirstStage  ( long limit = 0 ) = 0;
	virtual ZZ SecondStage ( long limit = 0 ) = 0;

protected:
	AlgebraicGroupPrimeStrategy& s;

	bool CheckGCD(ZZ& retval, ZZ_p& acc);
	bool AccumulateGCD(ZZ& retval, ZZ_p& residue);

	void ReportProgress();
	void ReportProgress(long last_p);
	void SetProgressSkip(double factor);

private:
	int gcd_skip, gcd_count;
	int base_report_skip, report_skip, report_count;

	TICKS	last_ticks;
};

