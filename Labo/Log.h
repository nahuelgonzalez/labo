#pragma once

#include "iostream"
#include "string"
#include "sstream"
#include "fstream"

using namespace std;


const std::string currentDateTime();


#define DEBUG_LOG(x) { extern bool __enable_debug_log; if ( __enable_debug_log ) cout << x; }

#define ADD_INDENT() { extern string __indent; __indent += "  "; }
#define REMOVE_INDENT() { extern string __indent; __indent = __indent.substr(0,__indent.size() - 2); }
#define LOG(x) { extern ofstream log_file; extern string __indent; stringstream __tmpstrstream; __tmpstrstream << currentDateTime() << __indent << " " << x << endl; cout << __tmpstrstream.str(); log_file << __tmpstrstream.str(); }
#define LOG_BEGIN(x) { cout << currentDateTime() << " " << x; }
#define LOG_CONTINUE(x) { cout << x; }
#define LOG_END() { cout << endl; }

#define PRINT_FACTORS(n) { LOG("Factoring " << n << "..."); ADD_INDENT(); StandardStrategy::GetInstance().PrintFactorization(n); REMOVE_INDENT(); }


void initialize_logs();

