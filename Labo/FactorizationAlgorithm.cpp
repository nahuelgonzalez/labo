#include "NTL/ZZ.h"
#include "NTL/ZZ_p.h"

using namespace NTL;

#include "FactorizationAlgorithm.h"
#include "Log.h"



FactorizationAlgorithm::FactorizationAlgorithm(ZZ& N, unsigned long op_skip)
	: N(N), timeout_stop(false)
{
	this->op_next = this->op_skip = op_skip;
	this->op_count = 0;
	last_ticks = MSTIMER();

	gcd_count = 0;
}


FactorizationAlgorithm::~FactorizationAlgorithm(void)
{
}


void FactorizationAlgorithm::Reset()
{
	timeout_stop = false;
}


void FactorizationAlgorithm::StartTimedExecution(long secs)
{
	timeout_stop = false;
	sleep_secs = secs;
	hThr = CreateThread(NULL, 0 , TimedExecutionThread, this, 0, NULL);
}


DWORD FactorizationAlgorithm::TimedExecutionThread(void* p)
{
	FactorizationAlgorithm* t = (FactorizationAlgorithm*) p;
	Sleep(1000 * t->sleep_secs);
	t->timeout_stop = true;
	return 0;
}


void FactorizationAlgorithm::Finish()
{
	TerminateThread(hThr, 0);
	timeout_stop = false;
	last_ticks = MSTIMER();
}


bool FactorizationAlgorithm::HasTimedOut()
{
	return timeout_stop;
}


void FactorizationAlgorithm::ReportOp()
{
	op_count++;
	if ( op_count > op_next )
	{
		op_next += op_skip;

		TICKS ticks = MSTIMER();
		unsigned long long ops = (unsigned long long) 1000 * op_skip/ (ticks - last_ticks);
		last_ticks = ticks;

		char tmp[100], tmp2[100];
		LOG("    #" << human_readable(tmp2,op_count) << " - " << human_readable(tmp,ops) << "ops/sec");
	}
}


bool FactorizationAlgorithm::AccumulateGCD(ZZ& retval, ZZ_p& acc)
{
	gcd_count++;
	if ( gcd_count > 100 )
	{
		gcd_count = 0;
		GCD(retval, rep(acc), N);
		return !IsOne(retval);
	}

	return false;
}


void FactorizationAlgorithm::Begin()
{
	start_ticks = MSTIMER();
}


DWORD FactorizationAlgorithm::GetRunningTime()
{
	return last_ticks - start_ticks;
}


unsigned long long FactorizationAlgorithm::GetTotalOps()
{
	return op_count;
}