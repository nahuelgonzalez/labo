#include "QuadraticFieldPPlusOne.h"
#include "common.h"


QuadraticFieldPPlusOne::QuadraticFieldPPlusOne(ZZ& N, AlgebraicGroupPrimeStrategy& s, ZZ& residue )
	: last_p(0), AlgebraicGroupFactorizationAlgorithm(N, s, 1)
{
	INIT_MODULUS(N);

	random(d);
	while ( IsZero(d) )
		random(d);

	random(a);
	while ( IsZero(a) )
		random(a);

	random(b);
	while ( IsZero(b) )
		random(b);

	ZZ_p den = a * a - d * b * b;
	ZZ_p num_a = a * a + d * b * b;
	ZZ_p num_b = - to_ZZ_p(2) * a * b;
	// TODO: verificar GCD de num_a y num_b con N
	a = num_a / den;
	b = num_b / den;

	RESTORE_MODULUS();
}


QuadraticFieldPPlusOne::~QuadraticFieldPPlusOne(void)
{
}


ZZ QuadraticFieldPPlusOne::FirstStage  ( long limit )
{
	INIT_MODULUS(N);
	SetProgressSkip(0.05);

	bool stop = false;
	ZZ retval = to_ZZ(1);

	long e;
	last_p = s.Next(&e);
	
	while ( !timeout_stop && !stop && (limit == 0 || last_p <= limit) )
	{
		for ( int i = 0 ; i < e ; i++ )
		{
			ZZ_p pow_a = a, pow_b = b;
			long mask = 2;

			if ( (last_p & 1) == 0 )
			{
				a = to_ZZ_p(1);
				b = to_ZZ_p(0);
			}

			do
			{
				ZZ_p last_a = pow_a, last_b = pow_b;
				pow_b *= last_a;
				pow_b *= to_ZZ_p(2);
				pow_a *= last_a;
				ZZ_p tmp = d;
				tmp *= last_b;
				tmp *= last_b;
				pow_a += tmp;

				if ( mask & last_p )
				{
					last_a = a;
					last_b = b;

					b *= pow_a;
					last_a *= pow_b;
					b += last_a;

					last_b *= pow_b;
					last_b *= d;
					a *= pow_a;
					a += last_b;					
				}
				
				mask <<= 1;
			}
			while ( mask < last_p );
		}

		last_p = s.Next(&e);
		stop = AccumulateGCD(retval, b);

		ReportProgress();
	}

	if ( !stop )
		CheckGCD(retval, b);

	RESTORE_MODULUS();
	Finish();
	return retval;
}


ZZ QuadraticFieldPPlusOne::SecondStage ( long limit )
{
	ZZ retval = to_ZZ(1);

	return retval;
}
