#pragma once

#include "NTL/ZZ.h"

using namespace NTL;


#include "list"

using namespace std;


typedef struct
{
	ZZ		p;
	long	e;
}
	PART;


class Factorization
{
public:
	Factorization(ZZ& N);
	~Factorization(void);

	bool IsComplete();
	void FactorFound(const ZZ& factor);

	void Print();
	ZZ	 GetRemainder();

	void AddWarning(string str);

private:
	ZZ	N;
	ZZ	remainder;
	list<PART*> parts;
	list<string> warnings;
};

