// Labo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "Random.h"
#include "TrialDivision.h"
#include "PollardPMinusOne.h"
#include "common.h"
#include "StandardStrategy.h"
#include "Log.h"

#include "cmath"
#include "windows.h"

void measure_log_speed()
{
	PrimeSeq seq;
	DWORD begin = GetTickCount();
	for ( int i = 0 ; i < 100000 ; i++ )
	{
		long p = seq.next();
		double lg = log((double) p);
	}
	DWORD end = GetTickCount();

	cout << "100000 ps/logs = " << (end - begin) << "ms." << endl;
}


void print_factors(ZZ n)
{
	LOG_BEGIN(n << " = ");

	TrialDivision t(n);
	while ( !t.IsReady() )
	{
		long e;
		long p = t.Next(&e, 100000); 

		if ( e == 1 )
			LOG_CONTINUE(p << " . ")
		else
			LOG_CONTINUE("^" << e << " . ");
	}

	LOG_END("");
}


int CheckGroup(long N)
{
	cout << "N = " << N << endl;

	int count = 0;
	for ( long i = 0 ; i < N ; i++ )
		for ( long j = 0 ; j < N ; j++ )
		{
			long r = ((i * i) % N + (j * j) % N) % N;
			if ( r == 1 )
			{
				count++;
				cout << "  (" << i << ", " << j << ")" << endl;
			}
		}

	cout << "total = " << count << endl;
	return count;
}


void FindOrder(long N, long a, long b)
{
	long order = 0;
	long x = a, y = b;
	do
	{
		int residue = (x*x + y*y) % N;
		if ( residue < 0 ) residue += N;

		cout << "  (" << x << ", " << y << ") " << " = " << x*x << " + " << y * y << " c= "
			 << residue << endl;
		long tx = (a*x - b*y) % N;
		long ty = (a*y + b*x) % N;
		if ( tx < 0 ) tx += N;
		if ( ty < 0 ) ty += N;

		//if ( (N - tx) < tx ) tx = N - tx;
		//if ( (N - ty) < ty ) ty = N - ty;

		//if ( tx > ty ) 
		//{
		//	long tmp = tx;
		//	tx = ty;
		//	ty = tmp;
		//}

		x = tx; y = ty;
		//getch();
		order++;
		// getch();
	}
	while ( x != a || y != b );

	cout << "order=" << order << endl;
}


void SearchQuads()
{
	PrimeSeq seq;
	for ( ; ; )
	{
		long count = 0;
		long p = seq.next();
		cout << "p = " << p << "    ";
		for ( long i = 0 ; i < p ; i++ )
			for ( long j = 0 ; j < p ; j++ )
			{
				long r = ((i * i) % p + (j * j) % p) % p;
				if ( r == 1 )
				{
					cout << "(" << i << ", " << j << ") ";
					count++;
				}
			}

		long expected = p - 1;
		if ( (p % 4) == 3 )
			expected = p + 1;

		cout << endl << "    (" << (p%4) << ", exp=" << expected << ") # = " << count << endl;
		getch();
	}
}


const int MAX_D = 1000;

void GetQuadraticRelation(ZZ& N, long d)
{
	long r = N % d;
	LOG("* r = " << r); 
	ADD_INDENT();

	bool is_residue[MAX_D];
	char str_residue[MAX_D + 1];
	for ( int i = 0 ; i < MAX_D ; i++ )
	{
		is_residue[i] = false;
		str_residue[i] = 'x';
	}

	str_residue[d] = 0;
	for ( long j = 1 ; j < d ; j++ )
	{
		long residue = (j * j) % d;
		is_residue[residue] = true;
		str_residue[residue] = 'o';
	}

	LOG("d = " << d << "  " << str_residue);

	long solutions[MAX_D];
	int solution_count = 0;
	for ( long j = 0 ; j < d ; j++ )
		if ( is_residue[j] && is_residue[(j - r + d) % d] )
		{
			solutions[solution_count] = j;
			solution_count++;
		}

	LOG("#solutions: " << solution_count);
	ADD_INDENT();
	for ( int i = 0 ; i < solution_count ; i++ )
		LOG("x = " << solutions[i] << " (mod " << d << ")");
	REMOVE_INDENT();
	REMOVE_INDENT();
}


void FermatFactorImprovements()
{
	const int PRIME_SIZE = 40;

	ZZ p1, p2;
	GenPrime(p1, PRIME_SIZE);
	GenPrime(p2, PRIME_SIZE);
	
	ZZ N = p1 * p2;
	ZZ a = (p1 + p2) / to_ZZ(2);
	ZZ b = (p1 - p2) / to_ZZ(2);

	LOG("N = " << N << " = " << a << "^2 + " << b << "^2");
	if ( N != (a*a - b*b) )
	{
		int k = 9;
	}

	LOG("Searching for relations...");
	ADD_INDENT();
	GetQuadraticRelation(N, 16);

	PrimeSeq seq;
	seq.next();
	while ( true )
	{
		long p = seq.next();
		GetQuadraticRelation(N, p*p);
		getch();
	}
}


void TestRho();

int _tmain(int argc, _TCHAR* argv[])
{
	INITIALIZE();
	TestRho();
	getch();
	return;
//	FermatFactorImprovements();
//	return 0;

	/*
	SearchQuads();	

	int order = 1;
	long a1 = 47, a2 = 61;
	long nu = a1 * a2;
	long phi = (a1 - 1) * (a2 - 1);
	order = CheckGroup(nu); // 23 * 29 parece diferir
	cout << "nu = " << nu << "  order = " << order << "    phi = " << phi << endl;
	FindOrder(nu, 2842, 1965);
	getch();
	*/
	//FindOrder (23 * 29, 195, 245);

	ZZ p1, p2;
	//ZZ p1 = to_ZZ("3700334136151"), p2 = to_ZZ("2298450584681");
	GenPrime(p1,55);
	GenPrime(p2,55);
	PRINT_FACTORS(p1-1);
	PRINT_FACTORS(p2-1);
	ZZ N = p1 * p2;

	LOG("p1=" << p1);
	LOG("p2=" << p2);
	LOG("N = " << N);

	//ZZ e = (p1 * 2 * InvMod(p1, p2) - 1) % N;
	//cout << "e = " << e << endl;
	//cout << "e^2 = " << (e*e)%N << endl;

	Factorization f = StandardStrategy::GetInstance().Factorize(N);
	f.Print();
	getch();

	/*
	PollardPMinusOne a ( N );
	ZZ factor = a.FirstStage(100000);

	if ( !IsOne(factor) )
		cout << "Factor found! k=" << factor << endl;
	else
	{
		cout << "Second stage..." << endl;
		factor = a.SecondStage();

		if ( a.HasTimedOut() )
			cout << "Timeout..." << endl;
		else
			cout << "Factor found! k=" << factor << endl;
	}
	*/
	getch();
	return 0;
}

