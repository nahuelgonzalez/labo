#include "stdio.h"


char* human_readable ( char* tmp, unsigned long val )
{
	if ( val < 1000 )
		sprintf ( tmp , "%ld" , val );
	else if ( val < 1000000 )
		sprintf ( tmp , "%.2fK" , (float) val / 1000.0 );
	else 
		sprintf ( tmp , "%.2fM" , (float) val / 1000000.0);

	return tmp;
}