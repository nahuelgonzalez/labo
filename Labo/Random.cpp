#include "Random.h"


ZZ RandomPrime(long size)
{
	ZZ retval;
	GenPrime ( retval , size );
	return retval;
}


ZZ RandomRSA(long size)
{
	ZZ p1, p2;
	GenPrime ( p1 , size );
	GenPrime ( p2 , size );
	return p1 * p2;
}