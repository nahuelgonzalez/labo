#pragma once

#include "Factorization.h"

#include "NTL/ZZ.h"

using namespace std;



class FactorizationStrategy
{
public:
	virtual Factorization Factorize(ZZ& N) = 0;

protected:
	FactorizationStrategy(void);
	~FactorizationStrategy(void);
};

