#include "PollardPMinusOne.h"
#include "common.h"
#include "stdio.h"


PollardPMinusOne::PollardPMinusOne(ZZ& N, AlgebraicGroupPrimeStrategy& s, ZZ& residue)
	: AlgebraicGroupFactorizationAlgorithm(N, s)
{
	INIT_MODULUS(N);
	this->residue = to_ZZ_p(residue);
	RESTORE_MODULUS();
}


PollardPMinusOne::~PollardPMinusOne(void)
{
}


ZZ PollardPMinusOne::FirstStage  ( long limit )
{
	SetProgressSkip(10);

	INIT_MODULUS(N);
	bool stop = false;
	ZZ retval = to_ZZ(1);

	long e;
	long last_p = s.Next(&e);
	
	while ( !timeout_stop && !stop && (limit == 0 || last_p <= limit) )
	{
		for ( int i = 0 ; i < e ; i++ )
			residue = power(residue, last_p);

		last_p = s.Next(&e);
		
		residue--;
		stop = AccumulateGCD(retval, residue);
		residue++;

		ReportProgress();
	}

	if ( !stop )
		CheckGCD(retval, residue - 1);
	//else
	//	N /= retval;

	RESTORE_MODULUS();
	Finish();
	return retval;
}


ZZ PollardPMinusOne::SecondStage ( long limit )
{
	SetProgressSkip(50);

	const int MAX_PRIME_GAP = 2000;
	INIT_MODULUS(N);

	ZZ retval = to_ZZ(1);
	ZZ_p differences[MAX_PRIME_GAP/2];
	int  max_difference = 0;

	ZZ_p b = residue * residue;
	differences[0] = b;
	residue = power(residue, s.BeginSecondStage());
	ZZ_p acc = (residue - 1);

	bool stop = false;
	while ( !stop && !timeout_stop && (limit == 0 || s.GetLastPrime() < limit) )
	{
		stop = AccumulateGCD(retval, acc);

		long gap = s.NextGap();

		int pos = gap / 2 - 1;
		if ( max_difference < pos )
		{
			if ( pos > MAX_PRIME_GAP/2 )
				throw new exception("blgblg");

			for ( int i = max_difference + 1; i <= pos ; i++ )
				differences[i] = differences[i-1] * differences[0];

			max_difference = pos;
		}

		residue *= differences[pos];
		residue--;
		acc *= residue;
		residue++;

		ReportProgress();
	}

	CheckGCD(retval, acc);
	RESTORE_MODULUS();
	Finish();
	return retval;
}


ZZ PollardPMinusOne::ImprovedSecondStage ( long w, long limit )
{
	INIT_MODULUS(N);
	SetProgressSkip(20);
	ZZ retval = to_ZZ(1);

	long last_p = s.BeginSecondStage();
	residue = power(residue, last_p);
	ZZ_p acc = residue;
	acc--;

	ZZ_p** b_u = (ZZ_p**) malloc(sizeof(ZZ_p*) * (w+1));
	for ( long i = 0; i <= w; i++ )
		b_u[i] = NULL;

	long vw = last_p + w;

	ZZ_p b_vw, b_w;
	power(b_w, residue, w);
	power(b_vw, residue, vw);

	bool stop = false;
	while ( !stop && !timeout_stop && (limit == 0 || s.GetLastPrime() < limit) )
	{
		long u = vw - s.NextSinglePrime();

		while ( u < 0 )
		{
			b_vw *= b_w;
			vw += w;
			u += w;
		}

		if ( b_u[u] == NULL )
		{
			b_u[u] = new ZZ_p();
			*b_u[u] = to_ZZ_p(1);
			power(*b_u[u], residue, u);
		}

		ZZ_p Ts = b_vw;
		Ts -= *b_u[u];
		acc *= Ts;

		stop = AccumulateGCD(retval, acc);
		ReportProgress();	
	}

	CheckGCD(retval, acc);
	RESTORE_MODULUS();
	Finish();
	return retval;
}

