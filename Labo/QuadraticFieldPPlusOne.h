#pragma once
#include "AlgebraicGroupFactorizationAlgorithm.h"

class QuadraticFieldPPlusOne : public AlgebraicGroupFactorizationAlgorithm
{
public:
	QuadraticFieldPPlusOne(ZZ& N, AlgebraicGroupPrimeStrategy& s, ZZ& residue = to_ZZ(2));
	~QuadraticFieldPPlusOne(void);

	virtual ZZ FirstStage  ( long limit = 0 );
	virtual ZZ SecondStage ( long limit = 0 );

private:
	ZZ_p a, b, d;
	long last_p;
};

