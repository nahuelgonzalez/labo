#pragma once
#include "AlgebraicGroupPrimeStrategy.h"
#include "NTL/ZZ.h"

using namespace NTL;


class MaxExponentStrategy : public AlgebraicGroupPrimeStrategy
{
public:
	MaxExponentStrategy(ZZ& N);
	~MaxExponentStrategy(void);

	virtual long BeginSecondStage();
	virtual long Next(long* e);

	virtual char* ReportProgress(char* tmp);

private:
	double		logN;
};

