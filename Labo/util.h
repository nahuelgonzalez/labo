#ifndef __UTIL_INCLUDED
#define __UTIL_INCLUDED

#ifdef WIN32
#include "windows.h"
#define MSTIMER()	GetTickCount()

typedef DWORD TICKS;
#endif

extern "C"
{
	char* human_readable ( char* tmp, unsigned long val );
}

#endif
