#ifndef __RANDOM_INCLUDED
#define __RANDOM_INCLUDED


#include "NTL/ZZ.h"

using namespace NTL;


ZZ RandomPrime(long size);
ZZ RandomRSA(long size);

#endif
