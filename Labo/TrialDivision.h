#pragma once

#include "NTL/ZZ.h"

using namespace NTL;


class TrialDivision
{
public:
	TrialDivision(ZZ& N);
	~TrialDivision(void);

	long Next(long* e, long bound);

	bool IsReady();
	long LastPrime();

private:
	ZZ N;
	PrimeSeq seq;
	long last_p;
};

