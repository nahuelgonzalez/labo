#pragma once
#include "NTL/ZZ.h"

using namespace NTL;


class AlgebraicGroupPrimeStrategy
{
public:
	virtual long Next(long* e) = 0;
	virtual long BeginSecondStage() = 0;
	virtual long NextGap();
	virtual long NextSinglePrime();
	virtual long GetLastPrime();

	unsigned long GetOpCount();

	virtual char* ReportProgress(char* tmp) = 0;

protected:
	AlgebraicGroupPrimeStrategy(void);
	~AlgebraicGroupPrimeStrategy(void);

	PrimeSeq	seq;
	long		last_prime;

	unsigned long op_count;
};

