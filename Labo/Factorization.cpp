#include "Factorization.h"
#include "common.h"


Factorization::Factorization(ZZ& N)
	: N(N), remainder(N)
{
}


Factorization::~Factorization(void)
{
}


bool Factorization::IsComplete()
{
	return IsOne(remainder);
}


void Factorization::FactorFound(const ZZ& factor)
{
	if ( (remainder % factor) != 0 )
	{
		cout << "UNHANDLED EXCEPTION: INVALID FACTOR (" << factor << ")" << endl;
		throw new exception("INVALID FACTOR");
	}

	bool found = false;
	for ( list<PART*>::iterator iter = parts.begin() ; iter != parts.end() ; ++iter )
	{
		if ( compare((*iter)->p, factor) == 0 )
		{
			found = true;
			(*iter)->e++;
			break;
		}
	}

	if ( !found )
	{
		PART* part = new PART();
		part->p = factor;
		part->e = 1;
		parts.push_back(part);
		// parts.sort(;
	}

	remainder /= factor;

	if ( !IsOne(factor) )
		LOG("  Factor found: " << factor);

	if ( ProbPrime(remainder) )
		FactorFound(remainder);
}


void Factorization::Print()
{
	int i = 0;
	int count = parts.size();
	LOG_BEGIN(N << " = ");
	for ( list<PART*>::iterator iter = parts.begin() ; iter != parts.end() ; ++iter, i++ )
	{
		LOG_CONTINUE((*iter)->p);
		if ( (*iter)->e != 1 )
			LOG_CONTINUE("^" << (*iter)->e);

		if ( i != (count - 1) )
			LOG_CONTINUE(" . ");
	}

	if ( !IsOne(remainder) )
		LOG_CONTINUE(" . (" << remainder << ")");

	LOG_END();
}


ZZ	 Factorization::GetRemainder()
{
	return remainder;
}


void Factorization::AddWarning(string str)
{
	warnings.push_back(str);
}
