#include "StandardStrategy.h"
#include "BrentRho.h"
#include "TrialDivision.h"
#include "PollardPMinusOne.h"
#include "MaxExponentStrategy.h"
#include "PowerPrimeStrategy.h"
#include "QuadraticFieldPPlusOne.h"
#include "Log.h"
#include "PollardRho.h"


StandardStrategy::StandardStrategy(void)
{
}


StandardStrategy::~StandardStrategy(void)
{
}


const int TRIAL_DIVISION_BOUND = 10000000;


void StandardStrategy::UseTrialDivision(ZZ N, Factorization& retval)
{
	TrialDivision td(N);
	while ( !retval.IsComplete() && td.LastPrime() < TRIAL_DIVISION_BOUND && !td.IsReady() )
	{
		long e;
		long p = td.Next(&e, TRIAL_DIVISION_BOUND);
		while ( e != 0 )
		{
			retval.FactorFound(to_ZZ(p));
			e--;
		}
	}
}


const int POLLARD_PMINUSONE_SECONDS = 30;


void StandardStrategy::UsePollardPMinusOne(ZZ N, Factorization& retval)
{
	bool repeat_st1 = true, repeat_st2 = false;
	PowerPrimeStrategy s(1000,3);
	PollardPMinusOne pollard (N, s);
	
	do
	{
		if ( repeat_st1 )
		{
			LOG("Pollard p-1 first stage...");
			repeat_st1 = false;
			pollard.StartTimedExecution(POLLARD_PMINUSONE_SECONDS);
			ZZ factor = pollard.FirstStage();
			if ( IsOne(factor) )
				repeat_st2 = true;
			else
			{
				retval.FactorFound(factor);
				repeat_st1 = true;
			}
		}
		
		if ( repeat_st2 )
		{
			LOG("Pollard p-1 second stage...");
			repeat_st2 = false;
			pollard.StartTimedExecution(POLLARD_PMINUSONE_SECONDS);
			//ZZ factor = pollard.SecondStage();
			ZZ factor = pollard.ImprovedSecondStage(100000);

			if ( !IsOne(factor) )
			{
				retval.FactorFound(factor);
				repeat_st2 = true;
			}
		}
	}
	while ( !retval.IsComplete() && (repeat_st1 || repeat_st2) );
}


Factorization StandardStrategy::Factorize(ZZ& N)
{
	Factorization retval(N);
	
	LOG("Trial division...");
	UseTrialDivision(N, retval);

	while ( !retval.IsComplete() )
	{
		BrentRho rho(retval.GetRemainder());
		LOG("Pollard rho...");
		ZZ factor = rho.Next();
		if ( !IsOne(factor) )
			retval.FactorFound(factor);
	}

	if ( !retval.IsComplete() )
		// UsePPlusOne(retval.GetRemainder(), retval);
		UsePollardPMinusOne(retval.GetRemainder(), retval);

	return retval;
}


StandardStrategy& StandardStrategy::GetInstance()
{
	static StandardStrategy instance;
	return instance;
}

void StandardStrategy::PrintFactorization(ZZ& N)
{
	Factorization f = Factorize(N);
	f.Print();
}


void StandardStrategy::UsePPlusOne(ZZ N, Factorization& retval)
{
	MaxExponentStrategy s(N);
	QuadraticFieldPPlusOne q(N, s);

	LOG("Quadratic field p+1 first stage...");
	q.StartTimedExecution(5);
	ZZ factor = q.FirstStage();
	if ( !IsOne(factor) )
		retval.FactorFound(factor);

	LOG("Quadratic field p+1 second stage...");
	factor = q.SecondStage();
	if ( !IsOne(factor) )
		retval.FactorFound(factor);
}
