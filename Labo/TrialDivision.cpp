#include "TrialDivision.h"
#include "Log.h"


TrialDivision::TrialDivision(ZZ& N)
	: N(N)
{
	last_p = 0;
}


TrialDivision::~TrialDivision(void)
{
}


long TrialDivision::Next(long* e, long bound)
{
	*e = 0;
	long p = 0;

	while ( *e == 0 && p < bound)
	{
		p = seq.next();
		long residue = (N % p);
		while ( residue == 0 )
		{
			(*e)++;

			N /= p;
			residue = (N % p);
		}
	}

	last_p = p;
	return p;
}


bool TrialDivision::IsReady()
{
	return IsOne(N);
}


long TrialDivision::LastPrime()
{
	return last_p;
}
