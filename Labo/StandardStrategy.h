#pragma once

#include "FactorizationStrategy.h"

class StandardStrategy : public FactorizationStrategy
{
public:
	virtual Factorization Factorize(ZZ& N);
	virtual void PrintFactorization(ZZ& N);

	static StandardStrategy& GetInstance();

private:
	StandardStrategy(void);
	~StandardStrategy(void);

	void UseTrialDivision(ZZ N, Factorization& retval);
	void UsePollardPMinusOne(ZZ N, Factorization& retval);
	void UsePPlusOne(ZZ N, Factorization& retval);
};

