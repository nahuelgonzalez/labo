#include "AlgebraicGroupPrimeStrategy.h"


AlgebraicGroupPrimeStrategy::AlgebraicGroupPrimeStrategy(void)
{
}


AlgebraicGroupPrimeStrategy::~AlgebraicGroupPrimeStrategy(void)
{
}


long AlgebraicGroupPrimeStrategy::NextGap()
{
	op_count++;
	long p = seq.next();
	long retval = p - last_prime;
	last_prime = p;
	return retval;
}


long AlgebraicGroupPrimeStrategy::NextSinglePrime()
{
	op_count++;
	last_prime = seq.next();
	return last_prime;
}


long AlgebraicGroupPrimeStrategy::GetLastPrime()
{
	return last_prime;
}


unsigned long AlgebraicGroupPrimeStrategy::GetOpCount()
{
	unsigned long retval = op_count;
	op_count = 0;
	return retval;
}
