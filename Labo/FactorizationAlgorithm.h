#pragma once

#include "windows.h"
#include "Log.h"
#include "util.h"

#include "NTL/ZZ.h"
#include "NTL/ZZ_p.h"

using namespace NTL;




class FactorizationAlgorithm
{
public:
	FactorizationAlgorithm(ZZ& N, unsigned long op_skip = 0);
	~FactorizationAlgorithm(void);

	void Reset();
	void StartTimedExecution(long secs);

	bool HasTimedOut();
	DWORD GetRunningTime();
	unsigned long long GetTotalOps();

protected:
	ZZ N;

	bool timeout_stop;

	bool AccumulateGCD(ZZ& retval, ZZ_p& acc);
	void ReportOp();

	void Begin();
	void Finish();

private:
	HANDLE	hThr;
	long	sleep_secs;

	static DWORD WINAPI TimedExecutionThread(void* p);

	unsigned long long op_skip;
	unsigned long long op_count;
	unsigned long long op_next;

	TICKS last_ticks, start_ticks;

	int gcd_count;
};

