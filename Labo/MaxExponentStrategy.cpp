#include "MaxExponentStrategy.h"


MaxExponentStrategy::MaxExponentStrategy(ZZ& N)
{
	logN = log(N);
}


MaxExponentStrategy::~MaxExponentStrategy(void)
{
}


long MaxExponentStrategy::BeginSecondStage()
{
	op_count = 0;
	return last_prime;
}


long MaxExponentStrategy::Next(long* e)
{
	op_count++;
	last_prime = seq.next();
	*e = logN / log((double) last_prime);
	return last_prime;
}

char* MaxExponentStrategy::ReportProgress(char* tmp)
{
	double e = logN / log((double) last_prime);
	sprintf ( tmp , "p = %ld (%ld)" , last_prime , (long) e );
	return tmp;
}
